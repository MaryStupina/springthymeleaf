package com.example.form;

import lombok.Data;

@Data
public class PersonForm {
    private String firstName;
    private String lastName;
}
